package com.Adhoc.testing;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TC_Actions_01 {

	public static void main(String[] args) throws IOException {
	
		   Logger log=LogManager.getLogger(TC_Actions_01.class);
	  	   BasicConfigurator.configure();
	  	   System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"\\driver\\msedgedriver.exe");
	  	   WebDriver driver=new EdgeDriver();
		   driver.manage().window().maximize();
	  	   FileReader reader=new FileReader(System.getProperty("user.dir")+"\\properties\\config.properties");
	  	   Properties p=new Properties();  
	  	   p.load(reader);
	  	   driver.get(p.getProperty("url")); 
		   log.info("demoqa webpage displayed");
		   driver.manage().window().maximize(); 
		   Actions actions = new Actions(driver);
		   WebElement btnElement =driver.findElement(By.xpath(p.getProperty("xpath")));
		   actions.doubleClick(btnElement).perform(); 
		   log.info("Button clicked");
		   log.info("Doubleclick Alert Accepted"); driver.close();
				 }
		  
		
		
	}


