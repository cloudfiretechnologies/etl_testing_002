package com.Adhoc.testing;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;

public class TC_Dragactions_02 {

	public static void main(String[] args) throws IOException {
		   Logger log=LogManager.getLogger(TC_Dragactions_02.class);
	  	   BasicConfigurator.configure();
	  	   System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"\\driver\\msedgedriver.exe");
	  	   WebDriver driver=new EdgeDriver();
		   driver.manage().window().maximize();
	  	   FileReader reader=new FileReader(System.getProperty("user.dir")+"\\properties\\drag.properties");
	  	   Properties p=new Properties();  
	  	   p.load(reader);
	  	   driver.get(p.getProperty("url")); 
		   log.info("demoqa webpage displayed");
		   driver.manage().window().maximize(); 
		   driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);		
		   Actions builder = new Actions(driver);
		   WebElement from = driver.findElement(By.xpath(p.getProperty("drag")));
		   WebElement to = driver.findElement(By.xpath(p.getProperty("drop")));
		   builder.dragAndDrop(from, to).perform();
		   String textTo = to.getText();
		   if(textTo.equals("Dropped!")) {
			log.info("PASS: Source could be dropped as expected");
		   }else {
			log.error("FAIL:source couldn't be dropped to target as expected");
		}   driver.close();
	}  
           
}
    