package com.smoke.tc_03;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TC_02_Write_col {
	
	public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException {
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook work=null;
		String fileExtensionName =fileName.substring(fileName.indexOf("."));
		if(fileExtensionName.equals(".xlsx")) {
			work=new XSSFWorkbook(inputStream);
			}else
				if(fileExtensionName.equals(".xls")) {
					work=new HSSFWorkbook(inputStream);
				}
		     Sheet sheet=work.getSheet(sheetName);
		     int rowCount=sheet.getLastRowNum()-sheet.getFirstRowNum();
		      Row row = sheet.getRow(0);
		      Row newRow=sheet.createRow(rowCount+1);
		       for (int j = 0; j < row.getLastCellNum(); j++) {
		    	Cell cell=newRow.createCell(j);
		    	cell.setCellValue(dataToWrite[j]);
		       }
		       inputStream.close();
		       FileOutputStream outputStream =new FileOutputStream(file);
		       work.write(outputStream);
		       outputStream.close();
	}
	
         public static void main(String...strings) throws IOException {
    	 String[] valueToWrite = {"Mr.H" ,"Kerala"};
    	 TC_02_Write_col objfile = new TC_02_Write_col();
		  objfile.writeExcel(System.getProperty("user.dir") +"\\testcase","Read.xlsx","smoke",valueToWrite);
		
}}


