package com.smoke.tc_02;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;

public class Tc_01_Framework_Data {
	

	public static void main(String[] args) throws IOException, InterruptedException {
		
		  System.setProperty(
		  "webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		  WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://demoqa.com/login");
		 
	    
	    
	    FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\testdata\\Book1.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
         XSSFSheet sheet =workbook.getSheetAt(2);
        // XSSFSheet sheet = workbook.getSheet("Sheet2");
       //  XSSFRow row = sheet.getRow(0);
         
         
        
        
         for (int i = 1; i <=sheet.getLastRowNum(); i++) {
        	XSSFRow row=sheet.getRow(i);
        	//System.out.println(row);
        	// for (int j =0; j <=row.getLastCellNum(); j++) {
        		
        	XSSFCell USERNAME=row.getCell(0);
        	String usrname=USERNAME.getStringCellValue();
        		
      // System.out.println(USERNAME);
       XSSFCell PASSWORD=row.getCell(1);
       String pwd=PASSWORD.getStringCellValue();
     //  System.out.println(PASSWORD);
       
       
       
		   WebElement user= driver.findElement(By.xpath("//input[1]"));
		   user.clear();
		   user.sendKeys(usrname);
		   WebElement pass=driver.findElement(By.xpath("//input[@type='password']"));
		   pass.clear();
		   pass.sendKeys(pwd);
		   WebElement login =driver.findElement(By.xpath("(//button[@class='btn btn-primary'])[1]"));
		   login.click();
		   
		    try {
			  String title = driver.getTitle();
			  if (title.equalsIgnoreCase("ToolsQA")) {
				   System.out.println("Testcase pass");
				  
			  }
			  WebElement logout =  driver.findElement(By.xpath("(//button[@id='submit'])[1]"));
			  logout.click();  
			  driver.navigate().refresh();
			   }
			   catch (NoSuchElementException e) {
				   String wrng_credential=driver.findElement(By.xpath("//p[@id='name']")).getText();
		           System.out.println("Testcase Fail");  	
		           System.out.println("Error message" +wrng_credential);
				  
				
			}}
			  
		
         }
	  
	   
	    
	    
	    
	    
	}
	



