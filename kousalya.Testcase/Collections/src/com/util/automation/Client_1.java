package com.util.automation;

import java.util.HashMap;
import java.util.Map;

public class Client_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer,Client> map=new HashMap<Integer,Client>();    
	    //Creating Books    
	   Client b1=new Client(101,789065444,"mishrak");
	   Client b2=new Client(201,960000000,"ishaan");   
	   Client b3=new Client(301,900222365,"harshi"); 
	    //Adding Books to map   
	    map.put(1,b1);  
	    map.put(2,b2);  
	    map.put(3,b3);  
	      
	    //Traversing map  
	    for(Map.Entry<Integer,Client> entry:map.entrySet()){    
	        int key=entry.getKey();  
	        Client b=entry.getValue();  
	        System.out.println(key+" Details:");  
	        System.out.println(b.id+" "+b.name+" "+b.number);   

	}

}
}