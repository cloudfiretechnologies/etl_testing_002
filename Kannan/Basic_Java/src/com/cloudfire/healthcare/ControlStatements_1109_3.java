package com.cloudfire.healthcare;

public class ControlStatements_1109_3 {

	public static void main(String[] args) {
		short year = 2011;
		/*if(year%4==0) {
			System.out.println("The given year is a LEAP year: "+year);
		}else {
			System.out.println("NOT A LEAP YEAR");
		}*/
		if((year%4==0)&&(year%100!=0)||(year%400==0)) {
			System.out.println("LEAP YEAR");
		}
		else {
			
			System.out.println("NOT A LEAP YEAR");
		}
		
		
	}

}
