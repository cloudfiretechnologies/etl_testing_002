SELECt    * FROM employeedetails;

describe employeedetails;

insert into employeedetails values ('121','John Snow','321',to_date('01-31-2014','mm-dd-yyyy'),'Tornoto');
insert into employeedetails values ('321','walter white','986',to_date('01-30-2015','mm-dd-yyyy'),'california');
insert into employeedetails values ('421','Kuldeep rana','876',to_date('11-27-2016','mm-dd-yyyy'),'new delhi');

describe employesalary;

  CREATE TABLE Employesalary (
  EmpId NUMBER(10),
  Project VARCHAR2(10),
  salary NUMBER(10),
  Variable NUMBER(4)  
);

insert into employesalary values ('121','P1','8000','500');
insert into employesalary values ('321','P2','10000','1000');
insert into employesalary values ('421','P1','12000','0');

select * from employesalary;
delete 
from
employesalary where empid=321;

1.	SQL Query to fetch records that are present in one table but not in another table.
 
select empid, fullname from employeedetails where empid not in (SELECT EMPID from employesalary); 
     
2.	SQL query to fetch all the employees who are not working on any project.



3.SQL query to fetch all the Employees from EmployeeDetails who joined in the Year 2020.
select * from EmployeeDetails where TO_CHAR(DateOfJoining,'YYYY') = '2020'; 

select * from EmployeeDetails where TO_CHAR(DateOfJoining,'YYYY') = '2016'; 

4.Fetch all employees from EmployeeDetails who have a salary record in EmployeeSalary.

SELECT E.FullName, S.Salary FROM EmployeeDetails E LEFT JOIN EmployeSalary S ON E.EmpId = S.EmpId;

5.	Write an SQL query to fetch project-wise count of employees.

SELECT Project, count(EmpId) employeecount FROM EmployeSalary group by Project;


6.	Fetch employee names and salary even if the salary value is not present for the employee.

SELECT E.FullName, S.Salary FROM EmployeeDetails E LEFT JOIN EmployeSalary S ON E.EmpId = S.EmpId;

7.	Write an SQL query to fetch all the Employees who are also managers.

SELECT DISTINCT E.FullName FROM EmployeeDetails E INNER JOIN EmployeeDetails M ON E.EmpID = M.ManagerID;

8.	Write an SQL query to fetch duplicate records from EmployeeDetails.

SELECT EMPID,FULLNAME,COUNT(*) FROM EMPLOYEEDETAILS GROUP BY EMPID,FULLNAME HAVING COUNT(*) > 1;

9.Write an SQL query to fetch only odd rows from the table.

select * from(select EMPID, FULLNAME, MANAGERID, rownum rn from EMPLOYEEDETAILS order by EMPID) where  mod (rn, 2) <> 0;

10.Write a query to find the 3rd highest salary from a table without top or limit keyword






