package com.smoke.tc_01;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TC_01_Read_Excel {
public static void main(String[] args) throws IOException {
	File file =new File("C:\\excel\\Book1.xlsx");
    FileInputStream fis = new FileInputStream(file);
    XSSFWorkbook workbook = new XSSFWorkbook(fis);
    XSSFSheet sheet = workbook.getSheet("Sheet1");
    XSSFRow row = sheet.getRow(0);
    int colNum = row.getLastCellNum();
    System.out.println("Total Number of Columns in the excel is : "+colNum);
    int rowNum = sheet.getLastRowNum()+1;
    System.out.println("Total Number of Rows in the excel is : "+rowNum);
    workbook.close();
}
}
