package com.cloudfire.String;

public class StringEndsWih {

	public static void main(String[] args) {
		String url = "https://opensource-demo.orangehrmlive.com";
		String suffix = "com";
		
		System.out.println(url.endsWith(suffix));
		System.out.println(url.endsWith("Com"));
		System.out.println(url.endsWith("M"));
		
		
		if(url.endsWith(suffix))
			System.out.println("valid");
		else
			System.out.println("Invalid");

	}

}
