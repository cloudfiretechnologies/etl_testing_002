package com.cloudfire.testbench;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cloudfire.config.Configuration;
import com.config.utils.Reports;
import com.config.utils.Util;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.CoordsProvider;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class TC054_TakeScreenshots_Ashot extends Util{
	static WebDriver driver = null;
	//static ExtentHtmlReporter htmlReporter;
	//public static ExtentReports extent;
	//public static ExtentTest test;
	public static ExtentReports extent;
	public static ExtentTest test;


	
	public TC054_TakeScreenshots_Ashot(WebDriver driver)
	{
		TC054_TakeScreenshots_Ashot.driver = driver;
	}

	public static void main(String[] args) throws IOException {
		extent = new ExtentReports(System.getProperty("user.dir")+File.separator+"Reports"+File.separator+"Ashot Report"+"_"+".html",false);

		extent.loadConfig(new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\config\\extent_config.xml"));


		
		ChromeOptions option = new ChromeOptions();
		option.addArguments("-incognito");
		option.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+Configuration.driver_path+"\\chromedriver.exe");
		driver = new ChromeDriver(option);
		//109.0.5414.120
		driver.navigate().to("https://shop.demoqa.com/my-account/");
		driver.manage().timeouts().implicitlyWait(5000,TimeUnit.MILLISECONDS);
		//WebElement lost_password = driver.findElement(By.linkText("Lost your password?"));
		WebElement password_lbl = driver.findElement(By.xpath("(//label[contains(text(),'Password')])[1]"));
		//(//label[contains(text(),'Password')])[1]
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true)", password_lbl);
		try {
		driver.findElement(By.xpath("//button[@name='login']")).click();
		}
		catch(StaleElementReferenceException e)
		{
		 e.printStackTrace();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//Take screenshot using Ashot
		//WebElement err_msg = driver.findElement(By.tagName("strong"));
		//WebElement err_msg = driver.findElement(By.xpath("(//li[normalize-space(text()='Username is required.')])[7]"));
		WebElement err_msg = driver.findElement(By.xpath("(//li[normalize-space(text()='Username is required.')])[7]/parent::ul"));
		
		
		System.out.println(err_msg.getText());
		Reports report = new Reports(driver);
		report.takeScreenshot_WebElement(err_msg, "gif");
		
		
		/*
		 * File destination_file = new
		 * File(System.getProperty("user.dir")+File.separator+"screenshots"+File.
		 * separator+"one.png"); Screenshot scrn_shot = new AShot().coordsProvider(new
		 * WebDriverCoordsProvider()).takeScreenshot(driver, err_msg); try {
		 * ImageIO.write(scrn_shot.getImage(),"png", destination_file); }
		 * catch(IOException e) { e.printStackTrace(); } catch(Exception e) {
		 * e.printStackTrace(); }
		 */
		//full page screenshot
		/*
		 * File target_loc = new
		 * File(System.getProperty("user.dir")+File.separator+"screenshots"+File.
		 * separator+"full_page.png"); Screenshot fullpage_screenshot = new
		 * AShot().shootingStrategy(ShootingStrategies.viewportPasting(5000)).
		 * takeScreenshot(driver); try { ImageIO.write(fullpage_screenshot.getImage(),
		 * "PNG", target_loc); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
		//driver.close();
				//PropertyConfigurator.configure(
				//System.getProperty("user.dir") + File.separator + "src\\main\\resources\\properties\\log4j.properties");
		Util.setExtent();
		//System.out.println(driver);
		test = extent.createTest("Full page screenshot");
		//report.takeScreenshot_fullpage("png");
		test.log(Status.PASS,"Test Case Passed");
		test.log(Status.PASS, test.addScreenCaptureFromPath(report.takeScreenshot_fullpage_report("jpg"));
		driver.close();
		Util.endReport();

		
	}

}
