package com.cloudfire.testbench;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.cloudfire.config.Configuration;

public class TC62_FluentWait {

	public static void main(String[] args) {

		System.setProperty("webdriver.edge.driver",
				System.getProperty("user.dir") + File.separator + "src\\main\\resources\\drivers\\msedgedriver.exe");
		//System.setProperty("webdriver.chrome.driver",
				//System.getProperty("user.dir") + File.separator + "src\\main\\resources\\drivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		WebDriver driver = new EdgeDriver();
		driver.get(Configuration.onlyURL);
		By fname = By.name("fname");
		WebElement first_name = driver.findElement(fname);
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		WebElement ele = wait.until(ExpectedConditions.visibilityOf(first_name));
		ele.sendKeys("Kannan" + " Baskar" + " Vivek" + " Prakash");
	}
}
