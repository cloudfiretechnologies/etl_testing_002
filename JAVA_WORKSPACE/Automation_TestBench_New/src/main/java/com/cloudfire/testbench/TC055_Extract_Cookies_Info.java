package com.cloudfire.testbench;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.Configuration;

public class TC055_Extract_Cookies_Info {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				Configuration.userdir + Configuration.driver_path + "chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(Configuration.url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		/*
		 * FluentWait<WebDriver> wait = new
		 * FluentWait<WebDriver>(driver).withTimeout(50,TimeUnit.SECONDS).
		 * pollingEvery(50,TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		 */ // Enter User Name
		WebElement uname = driver.findElement(By.xpath("//input[@name='username']"));
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='username']")));
		uname.clear();
		uname.sendKeys(Configuration.orgUname);
		// Enter Password
		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']")));
		pwd.clear();
		pwd.sendKeys(Configuration.orgPwd);
		// Click Login Button
		WebElement btnLogin = driver.findElement(By.xpath("//button[@type='submit']"));
		btnLogin.click();
		Set<Cookie> totalCookies = driver.manage().getCookies();
		System.out.println("Total Number of Cookies: " + totalCookies.size());

		for (Cookie cookie : totalCookies) {
			System.out.println(String.format("%s->%s->%s->%s", "Domanin Name: " + cookie.getDomain(),
					" Cookie Name: " + cookie.getName(), " Cookie Value: " + cookie.getValue(),
					" Cookie Expiry: " + cookie.getExpiry()));
		}

		Cookie cookie1 = new Cookie("MyCookie1", "15a5aa9a61930e569e05e8c5077f55e6");
		Cookie cookie2 = new Cookie("MyCookie2", "15a5aa9a61930e569e05e8c5077f55e9");
		driver.manage().addCookie(cookie1);
		driver.manage().addCookie(cookie2);

		Set<Cookie> totalCookies2 = driver.manage().getCookies();
		System.out.println("Total Number of Cookies: " + totalCookies2.size());

		for (Cookie cookie : totalCookies2) {
			System.out.println(String.format("%s->%s->%s->%s", "Domanin Name: " + cookie.getDomain(),
					" Cookie Name: " + cookie.getName(), " Cookie Value: " + cookie.getValue(),
					" Cookie Expiry: " + cookie.getExpiry()));


		}

		driver.manage().deleteCookieNamed("MyCookie1");

		Set<Cookie> totalCookies3 = driver.manage().getCookies();
		System.out.println("Total Number of Cookies: " + totalCookies3.size());

		for (Cookie cookie : totalCookies3) {
			System.out.println(String.format("%s->%s->%s->%s", "Domanin Name: " + cookie.getDomain(),
					" Cookie Name: " + cookie.getName(), " Cookie Value: " + cookie.getValue(),
					" Cookie Expiry: " + cookie.getExpiry()));
		}

		//Delete all cookies
		driver.manage().deleteAllCookies();

		Set<Cookie> totalCookies4 = driver.manage().getCookies();
		System.out.println("Total Number of Cookies: " + totalCookies4.size());

		for (Cookie cookie : totalCookies4) {
			System.out.println(String.format("%s->%s->%s->%s", "Domanin Name: " + cookie.getDomain(),
					" Cookie Name: " + cookie.getName(), " Cookie Value: " + cookie.getValue(),
					" Cookie Expiry: " + cookie.getExpiry()));
		}



		if (driver != null) {
			driver.close();
		}
	}

}

