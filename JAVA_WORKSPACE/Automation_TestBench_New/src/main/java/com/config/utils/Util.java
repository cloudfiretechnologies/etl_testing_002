package com.config.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cloudfire.config.Configuration;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.jna.platform.FileUtils;
import com.sun.tools.sjavac.Log;

public class Util {

	public static WebDriver driver = null;
	private static Logger log = Logger.getLogger(Util.class.getName());
	public static Reports report;


	static String url = "";
	static int responsecode = 0;
	//static ExtentHtmlReporter htmlReporter;
	////protected static ExtentReports extent;
	//protected static ExtentTest test;
	 public static ExtentReports extent;
	 public static  ExtentTest test;

	public static int zoomValue = 100;;
	public static int zoomIncrement;
	public static int zoomDecrement;
	public static int level;

	public static void datePicker(String date) {

		//test = extent.createTest("Date Picker Testcase");
		PropertyConfigurator.configure(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\properties\\log4j.properties");
		//Util.browserName("GC", "http://only-testing-blog.blogspot.com/2014/09/selectable.html");
		log.info("Browser launched successfully");
		WebElement datepicker = driver.findElement(By.xpath("//input[@id='datepicker' or @type = 'text']"));
		datepicker.click();
		log.info("Date Picker clicked");		
		WebElement datepickerElement = driver.findElement(By.xpath(
				"//div[@id='ui-datepicker-div' or @class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all']"));
		List<WebElement> datepickerElement_lst = datepickerElement.findElements(By.tagName("a"));
		//test.log(Status.INFO, "Date Picker Selected successfully");
		log.info("Date Picker Selected Successfully"); 

		outer: for (WebElement webElement : datepickerElement_lst) {
			if (webElement.getText().trim().equals(date)) {
				webElement.click();
				break outer;
			}
		}
		//test.log(Status.PASS, "The expected date is selected and test case passed");
		//log.info("The expected date is selected and test case passed");
	}

	
	  public static void datePickerFuture(String date, String month_year) {
	  
	  //test = extent.createTest("Date Picker Testcase-2");
	  PropertyConfigurator.configure( System.getProperty("user.dir") +
	  File.separator + "src\\main\\resources\\properties\\log4j.properties");
	  
	  Util.setExtent();
	  WebDriver driver = Util.multiBrowser("GC");
	 driver.get("http://only-testing-blog.blogspot.com/2014/09/selectable.html");
	 log.info("Browser Launched Successfully"); 
	 test.log(LogStatus.INFO, "Browser Launched Successfully"); 
	  WebElement datepicker =
	  driver.findElement(By.xpath(Util.readData_Property("datepicker","login")));
	  datepicker.click(); 
	  log.info("Date Picker Clicked");
	  test.log(LogStatus.INFO,"Date Picker Clicked"); 
	  WebDriverWait wait = new
	  WebDriverWait(driver, 30); String expectedmonthyr =
	  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath
	  (Util.readData_Property("expectedmonthyr","login")))).getText();
	  log.info("Current Month and Year: "+expectedmonthyr);
	  test.log(LogStatus.INFO,"Current Month and Year : "+expectedmonthyr); 
	  String
	  month_year2 = month_year; 
	  String date1 = date; 
	  String monyr=null; 
	  while(true) 
	  { monyr = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
	  "//div[@class='ui-datepicker-title']"))) .getText(); 
	  if
	  (monyr.equals(month_year)) { break;
	  
	  } 
	  else { wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
	  "//span[text()='Next']/parent::a"))).click(); 
	  }
	  
	  }
	 		List<WebElement> lstele = driver.findElements(By.xpath("(//div//table)[2]//tbody/tr/td/a"));

	
	  String adate = null; for (WebElement webElement : lstele) { adate =
	  webElement.getText(); if (adate.equals(date)) { webElement.click(); break;
	  
	  }
	  }
	  
	  }
	 
		//test.log(Status.INFO, "Expected Future date is picked successfully : "+adate+" "+monyr);
		//log.info("Expected Future date is picked successfully : "+adate+" "+monyr);

	//}

	//public void dropDown(String xpath, String text, WebDriver driver) throws InterruptedException {
		//Util.driver = driver;

		//WebElement ele = driver.findElement(By.xpath(xpath));
		//ele.click();
		//Thread.sleep(2000);
		//ele.findElement(By.xpath("//*[text()='" + text + "']")).click();

	//}

	public static WebDriver multiBrowser(String bsname) {

		if (bsname.equalsIgnoreCase("GC") )
				{
			ChromeOptions option = new ChromeOptions();
			option.addArguments("-incognito");
			option.addArguments("--start-maximized");
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + Configuration.driver_path
					+ File.separator + "chromedriver.exe");
			driver = new ChromeDriver(option);
			//report = new Reports(driver);
			//report.takeScreenShot(Configuration.userdir+File.separator+"screenshots"+File.separator, "login_page.png");

				}else if(bsname.equalsIgnoreCase("EDGE"))
						{
					EdgeOptions opt = new EdgeOptions();
					opt.setCapability(CapabilityType.BROWSER_NAME, BrowserType.EDGE);
					opt.setCapability("--start-maximized", true);

					// opt.addArguments("-incognito");
					// opt.addArguments("--start-maximized");
					System.setProperty("webdriver.edge.driver", System.getProperty("user.dir") + Configuration.driver_path
							+ File.separator + "msedgedriver.exe");
					driver = new EdgeDriver();
					}
				else if(bsname.equalsIgnoreCase("HTMLUNIT")) {
					driver = new HtmlUnitDriver();
				}
				else if(bsname.equalsIgnoreCase("FF"))
					{
						
					}
				else {
					System.out.println("Invalid Browser");
				}
				
				return driver;
	}

	public static void chromeBrowserLaunch(String link) {
		ChromeOptions option = new ChromeOptions();
		option.addArguments("-incognito");
		option.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + Configuration.driver_path + File.separator + "chromedriver.exe");
		driver = new ChromeDriver(option);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(link);
		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);

	}

	public static void orangeHRMLogin() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		Thread.sleep(5000);
		WebElement user_name = driver.findElement(By.name(Configuration.uname));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(Configuration.uname)));
		user_name.sendKeys("Admin");
		WebElement p_word = driver.findElement(By.name(Configuration.pwd));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(Configuration.pwd)));
		p_word.sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

	public static void brokenLinksURL() {

		int validlnkcount = 0;
		int brokenlnkcount = 0;

		List<WebElement> links = driver.findElements(By.tagName("a"));
		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {
			WebElement webElement = (WebElement) it.next();
			url = webElement.getAttribute("href");

			if (url == null || url.isEmpty()) {
				System.out.println(url + "==>URL is not configured or empty");
				continue;
			}

			try {
				HttpURLConnection huc = (HttpURLConnection) new URL((String) url).openConnection();
				huc.setRequestMethod("HEAD");
				responsecode = huc.getResponseCode();
				System.out.println("ResponseCode: " + responsecode);

			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (responsecode >= 400) {
					System.out.println(url + "==>is a Broken Link" + " & Its ResponseCode:==>" + responsecode);
					brokenlnkcount++;
				} else {
					System.out.println(url + "==>is a Valid Link" + " & Its ResponseCode:==>" + responsecode);
					validlnkcount++;
				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Valid Link Count = " + validlnkcount);
		System.out.println("Broken Link Count = " + brokenlnkcount);

	}

	public static void brokenLinksImage() {
		int validlnkimgcount = 0;
		int brokenlnkimgcount = 0;

		List<WebElement> links = driver.findElements(By.tagName("img"));
		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {
			WebElement webElement = (WebElement) it.next();
			url = webElement.getAttribute("src");

			if (url == null || url.isEmpty()) {
				System.out.println(url + "==>URL is not configured or empty");
				continue;
			}

			try {
				HttpURLConnection huc = (HttpURLConnection) new URL((String) url).openConnection();
				huc.setRequestMethod("HEAD");
				responsecode = huc.getResponseCode();
				System.out.println("ResponseCode: " + responsecode);

			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (responsecode >= 400) {
					System.out.println(url + "==>is a Broken Link Image" + " & Its ResponseCode:==>" + responsecode);
					brokenlnkimgcount++;
				} else {
					System.out.println(url + "==>is a Valid Link Image" + " & Its ResponseCode:==>" + responsecode);
					validlnkimgcount++;
				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Valid Link Image Count = " + validlnkimgcount);
		System.out.println("Broken Link Image Count = " + brokenlnkimgcount);
	}

	/*
	 * public static void setExtent() { htmlReporter = new ExtentHtmlReporter(
	 * System.getProperty("user.dir") + File.separator + "Reports" + File.separator
	 * + "ExtentReport.html");
	 * htmlReporter.config().setDocumentTitle("Automation Report");
	 * htmlReporter.config().setReportName("Functional Test");
	 * htmlReporter.config().setTheme(Theme.DARK); extent = new ExtentReports();
	 * extent.attachReporter(htmlReporter); extent.setSystemInfo("Hostname",
	 * "LocalHost"); extent.setSystemInfo("OS", "Windows10");
	 * extent.setSystemInfo("Tester Name", "Kannan");
	 * extent.setSystemInfo("Browser", "Chrome");
	 * 
	 * }
	 */
	
	
	  public static void setExtent() 
	  { 
		  
		  extent = new ExtentReports(System.getProperty("user.dir")+File.separator+"Reports"+File.separator+"ExtentReport.html", false);
			extent.loadConfig(new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\config\\extent_config.xml"));
			//Pre-conditions
			test = extent.startTest("Build Verification");
			test.assignAuthor("Kannan");
			test.assignCategory("SmokeReports - Production");
			
		  
		  	  
	  }
	 
	
	public static void endReport() {
		//Post-conditions
		extent.endTest(test);
		driver.close();
		test.log(LogStatus.PASS, "Browser closed successfully");
		extent.flush();
		extent.close();
	}

	public static void zoomIn_JS() {
		//test = extent.createTest("Zoom In");
	//////	test.log(Status.INFO, "ZoomIn Test Case");
		zoomValue = zoomValue + 100;
		zoom(zoomValue);
		System.out.println(zoomValue);
		//test.log(Status.PASS, "Zoom In Passed");

	}

	/*
	 * public static void zoomOut_JS() { test = extent.createTest("Zoom Out");
	 * test.log(Status.INFO, "Zoom Out Testcase"); zoomValue = zoomValue - 150;
	 * zoom(zoomValue); System.out.println(zoomValue); test.log(Status.PASS,
	 * "Zoom Out Passed"); }
	 * 
	 * public static void zoom_100_Percent() { test =
	 * extent.createTest("100% Zoom"); test.log(Status.INFO, "100% Zoom Testcase");
	 * zoomValue = zoomValue + 50; zoom(zoomValue); System.out.println(zoomValue);
	 * test.log(Status.PASS, "100% Zoom Passed"); }
	 */
	public static void zoom(int level) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='" + level + "%'", "");

	}

	//Read XPath from Properties file
	public static String readData_Property(String key,String filename)
	{
		FileReader file = null ;
		try {
			file = new FileReader(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\properties"+File.separator+filename+".properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop.getProperty(key);
	}


	public static void endSession() {
		driver.close();
	}

}