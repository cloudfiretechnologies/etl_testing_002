package com.config.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;


public class Reports {

	public WebDriver driver;

	public Reports(WebDriver driver) {
		this.driver = driver;
	}
	
	public void takeScreenshot_WebElement_AShot(WebElement element,String image_format)
	{
		File destination_file = new File(System.getProperty("user.dir")+File.separator+"screenshots"+File.separator+getTimeStamp()+"."+image_format);
		//Screenshot scrn_shot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, element);
		try {
		//ImageIO.write(scrn_shot.getImage(),"png", destination_file);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
//	File target_loc;
	public void takeScreenshot_fullpage(String image_format)
	{
		File target_loc = new File(System.getProperty("user.dir")+File.separator+"screenshots"+File.separator+getTimeStamp()+"."+image_format);
		TakesScreenshot fullpage_screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(5000)).takeScreenshot(driver);
		try {
			ImageIO.write(fullpage_screenshot.getImage(), "PNG", target_loc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//File File;
		//return target_loc;
	}
	public String takeScreenshot_fullpage_report(String image_format)
	{
		File target_loc = new File(System.getProperty("user.dir")+File.separator+"screenshots"+File.separator+getTimeStamp()+"."+image_format);
		Screenshot fullpage_screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(5000)).takeScreenshot(driver);
		try {
			ImageIO.write(fullpage_screenshot.getImage(), "PNG", target_loc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String imagePath = target_loc.getAbsolutePath();
		return imagePath;
		
		//File File;
		//return target_loc;
	}
	
	public String getTimeStamp()
	{
		String dateformat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss-SSS").format(new Date());
		return dateformat;
		
	}
	
	//take screen shot only for failed testcases
	public String takeScreenShot_Failures_WithTimeStamp(String filepath,String fileformat)
	{
		File target_file = new File(filepath+getTimeStamp()+"."+fileformat);
		TakesScreenshot scr_shot = (TakesScreenshot)driver;
		
		try {
			File source_file=scr_shot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source_file, target_file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		catch(WebDriverException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		String err_file_path = target_file.getAbsolutePath();
		
		return err_file_path;
		
	}
	
	
	
	
	public void takeScreenShot_WithTimeStamp(String filepath,String fileformat)
	{
		File target_file = new File(filepath+getTimeStamp()+fileformat);
		TakesScreenshot scr_shot = (TakesScreenshot)driver;
		File source_file=scr_shot.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source_file, target_file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	public void takeScreenShot(String filepath, String filename) {
		File target_file = new File(filepath + filename);
		TakesScreenshot scrshot = (TakesScreenshot) driver;

		// FileUtils.copyFile(source_file,target_file);
		try {
			File source_file = scrshot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source_file, target_file);

			// FileHandler.copy(target_file, source_file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/*
	 * public static void main(String[] args) {
	 * 
	 * Reports report = new Reports(driver);
	 * report.takeScreenShot(System.getenv("user.dir")+File.separator+"screenshots",
	 * "login_page.png");
	 */

}
