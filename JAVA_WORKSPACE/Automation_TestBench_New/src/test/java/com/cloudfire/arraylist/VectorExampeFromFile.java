package com.cloudfire.arraylist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Vector;

public class VectorExampeFromFile {
	
	static String filepath = System.getProperty("user.dir") + File.separator + "src\\test\\resources\\Vector.txt";

	public Vector<String> vectorMethod(String filepath) throws IOException {
		File file = null;
		FileReader fr = null;
		BufferedReader br = null;
		String line = null;
		Vector<String> al = new Vector<String>();

		
		try {
			file = new File(filepath);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			while ((line = br.readLine())!= null) {
				al.add(line);
			}
			return al;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	public static void main(String[] args) throws IOException {

		VectorExampeFromFile obj = new VectorExampeFromFile();
		System.out.println(obj.vectorMethod(filepath));

	}

}
