package com.cloudfire.collection;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class LinkedHashMapExample1 {

	public static void main(String[] args) {
		Map<String,Integer> m = new LinkedHashMap<String,Integer>();
		m.put("Ganesan", 1);
		m.put("Murugan", 2);
		m.put(null, null);
		m.put("Ram", null);
		m.put("Murugansamy", 2);
		m.put("Murugansamy", 2);
		
		for(Map.Entry<String,Integer> me: m.entrySet())
	            {   
	                System.out.print(me.getKey() + ":");   
	                System.out.println(me.getValue());   
	            }
		System.out.println("After invoking putIfAbsent method:");
		m.putIfAbsent("Kannan", 100);
		for(Map.Entry<String,Integer>me:m.entrySet())
		{
			System.out.println(me.getKey()+": "+me.getValue());
		}
		
		LinkedHashMap<String,Integer> hm = new LinkedHashMap<String, Integer>();
		hm.put("Madhes",100);
		hm.putAll(m);
		System.out.println("After Invoking putAll method: ");
		for(Map.Entry<String, Integer> hm1 :hm.entrySet())
		{
			System.out.println("Key=====>"+hm1.getKey()+" | Value====>"+hm1.getValue());
		}
		System.out.println("Key Value Pair: ===============>"+hm.entrySet());
		hm.remove(null,null);
		System.out.println("Key Value Pair: ===============>"+hm.entrySet());
		

	}

}
