package com.cloudfire.multithreading;

public class Thread_Creation_6 extends Thread{
	
	public void run()
	
	{
		for (int i = 0; i < 10; i++) {
			System.out.println(i+"------"+ Thread.currentThread().getName());
			
		}
	}

	public static void main(String[] args) {
		Thread_Creation_6 th0 = new Thread_Creation_6();
		Thread_Creation_6 th1 = new Thread_Creation_6();
		Thread_Creation_6 th2 = new Thread_Creation_6();
		
		th0.start();
		try {
			th0.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		th1.start();
		th2.start();
		
	}

}
