package com.cloudfire.healthcare;

public class DecisionMakingStatement4_ATF19 {
	public static void main(String[] args) {
		char branchname = 'C';
		byte studyingyear = 1;

		switch(studyingyear)
		{
		case 1: 
			switch(branchname)
			{
			case 'C': 
				System.out.println("1YEAR-Computer Science: Maths, Physics, Chemistry");
				break;
			case 'A': 
				System.out.println("1YEAR-Automobile: Car Desigining");
				break;
			case 'M': 
				System.out.println("1YEAR-Mechanical: Car Mechanism");
				break;
			case 'E': 
				System.out.println("ECE: Thermodynamics");
				break;

			default: 
				System.out.println("1YEAR-Invalid branch");

			}
			break;
		case 2: 
			switch(branchname)
			{
			case 'C': 
				System.out.println("2YEAR-Computer Science: Maths, Physics, Chemistry");
				break;
			case 'A': 
				System.out.println("2YEAR-Automobile: Car Desigining");
				break;
			case 'M': 
				System.out.println("2YEAR-Mechanical: Car Mechanism");
				break;
			case 'E': 
				System.out.println("2YEAR-ECE: Thermodynamics");
				break;

			default: 
				System.out.println("2YEAR-Invalid branch");

			}
			break;
		case 3: 
			switch(branchname)
			{
			case 'C': 
				System.out.println("3YEAR-Computer Science: Maths, Physics, Chemistry");
				break;
			case 'A': 
				System.out.println("3YEAR-Automobile: Car Desigining");
				break;
			case 'M': 
				System.out.println("3YEAR-Mechanical: Car Mechanism");
				break;
			case 'E': 
				System.out.println("3YEAR-ECE: Thermodynamics");
				break;

			default: 
				System.out.println("3YEAR-Invalid branch");

			}
			break;
		case 4: 
			switch(branchname)
			{
			case 'C': 
				System.out.println("4YEAR-Computer Science: Maths, Physics, Chemistry");
				break;
			case 'A': 
				System.out.println("4YEAR-Automobile: Car Desigining");
				break;
			case 'M': 
				System.out.println("4YEAR-Mechanical: Car Mechanism");
				break;
			case 'E': 
				System.out.println("4YEAR-ECE: Thermodynamics");
				break;

			default: 
				System.out.println("4YEAR-Invalid branch");

			}
			break;
		default:
			System.out.println("Invalid year: or Invalid Branch");
		}	
	}
}
