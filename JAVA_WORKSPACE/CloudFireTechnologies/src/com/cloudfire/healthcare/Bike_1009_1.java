package com.cloudfire.healthcare;

public class Bike_1009_1 {

	public static void main(String[] args) {
		//4 byte
		int cost = 49000;
		String model = "2010";
		//4 byte
		float petrol_price = 103.50f;
		//1 bit
		boolean bikeInsuranceLive = true;
		//1 byte
		byte bikeParkingCharge = 100;
		//2 byte
		char logo = 'H';
		//2 byte
		short sh = 32767;
		//8 byte
		long lng = 9223372036854775807l;
		//8 byte
		double dbl = 9223372036854775808.00d;

		int val = bikeCost(cost,model);
		System.out.println("BIKE PURCHASED FOR Rs: "+val);
		//(boolean bike, int costing,char a, byte park,short s,float desimal,double twodigit
		allDataTypesPrinting(bikeInsuranceLive,cost,logo,bikeParkingCharge,sh,petrol_price,dbl,lng);
	}
	public static int bikeCost(int price, String year) {
		String yr = year;
		System.out.println("My BIKE Purchased in the year of: "+yr);
		return price;

	}
	public static void allDataTypesPrinting(boolean bike, int costing,char a, byte park,short s,float desimal,double twodigit,long lg) {
		System.out.print("Insurance: "+bike+"/ Bike Cost:  "+costing+"/ Logo: "+a+"/ Parking Charge: "+park+"/ Short Value: "+s+"/ Float : "+desimal+"/ Double: "+twodigit+"/ Long value: "+lg);
	}
}
