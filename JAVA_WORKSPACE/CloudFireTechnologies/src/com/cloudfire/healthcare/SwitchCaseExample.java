package com.cloudfire.healthcare;

public class SwitchCaseExample {

	public static void main(String[] args) {
		int dayoftheweek = 3;
		String nameoftheday = "";
		
		switch (dayoftheweek) {
		case 1:
			nameoftheday = "Sunday";
			break;
		case 2:
			nameoftheday = "Monday";
			break;
		case 3:
			nameoftheday = "Tuesday";
			//break;
		case 4:
			nameoftheday = "Wednesday";
			//break;
		case 5:
			nameoftheday = "Thursday";
			//break;
		case 6:
			nameoftheday = "Friday";
			//break;
		case 7:
			nameoftheday = "Saturday";
			//break;

		default:
			System.out.println("It is Default statement");
			//break;
		}
		System.out.println("Name of the Day: "+nameoftheday);
	}

}
