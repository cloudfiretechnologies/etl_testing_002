package com.cloudfire.healthcare;

public class NestedForLoop3 {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) 
		{
			for (int j = 3; j >= i; j--) 
			{
				System.out.print("*");

			}
			System.out.println();

		}

	}
}
