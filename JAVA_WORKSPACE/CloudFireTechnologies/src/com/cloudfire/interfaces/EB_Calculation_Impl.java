package com.cloudfire.interfaces;

public abstract class EB_Calculation_Impl implements EB_Calculation{
	float netunit;
	public float domesticEBBill(float no_of_unit)
	{
		float billamt = 0;
		if(no_of_unit>0 && no_of_unit<=100)
		{
			//0-100 units = 0
			billamt = no_of_unit * 0;
		}
		else if(no_of_unit>=101 && no_of_unit<=200)
		{
			/*
			 * 0-100 units = 0 
			 * 101-200 units =1.50
			 */
			netunit = (no_of_unit - 100);
			billamt = (float) ((netunit)* 1.50);

		}
		else if(no_of_unit>=201 && no_of_unit<=500)
		{
			/*
			 * 0-100 units = 0 
			 * 101-200 units = 2.00 
			 * 201-500 units = 3.00
			 */
			netunit = (no_of_unit - 100);
			billamt = (float) ((100 * 2.00) + ((netunit-100) * 3.00));

		}
		else if(no_of_unit>=501)
		{
			/*
			 * 0-100 units = 0
			 * 101-200 units = 3.50 //100
			 * 201-500 units = 4.60 //300
			 * above 500 units = 6.60
			 * 			
			 */
			netunit = (no_of_unit - 100);
			billamt = (float) (((100 * 3.50) + ((300) * 4.60) + ((netunit-400)*6.60)));
		}
		return billamt;




	}

	public float IndustriesEBBill(float no_of_unit) 
	{ 
		float billamt = 0; 
		billamt = (float) (no_of_unit * 6.35); return billamt; 
	}

}
