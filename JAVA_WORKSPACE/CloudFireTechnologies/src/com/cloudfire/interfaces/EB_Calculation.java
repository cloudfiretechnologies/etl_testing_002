package com.cloudfire.interfaces;

public interface EB_Calculation {

	public abstract float domesticEBBill(float no_of_unit);
	public abstract float IndustriesEBBill(float no_of_unit);

}
