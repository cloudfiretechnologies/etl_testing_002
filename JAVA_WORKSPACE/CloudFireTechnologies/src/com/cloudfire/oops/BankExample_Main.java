package com.cloudfire.oops;

public class BankExample_Main {

	public static void main(String[] args) {
		BankExample obj_be = new BankExample();
		
		obj_be.variable_initialization(369, "Kannan", 50000.0f);
		System.out.println("BalanceAfterDeposit - "+obj_be.deposit(80000.0f));
		System.out.println("BalanceAfterWithdraw- "+obj_be.withdraw(30000.0f));
		obj_be.balance_check();
	}

}
