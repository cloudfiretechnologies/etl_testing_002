package com.cloudfire.oops;

public class BankExample {

	int acct_no;
	String acct_holder_name;
	float bank_balance;
	float amount;
	float bal;

	public void variable_initialization(int acct_no,String acct_holder_name,float bank_balance )
	{
		int act = acct_no;
		String name = acct_holder_name;
		bal = bank_balance;

		System.out.println("Account No:"+act+"::Account Holder Name:"+name+"::Balance: "+bank_balance);
	}
	float deposit(float amount)
	{
		bal = bal + amount;
		return bal;
	}
	float withdraw(float amount)
	{
		bal = bal - amount;
		return bal;
	}
	void balance_check()
	{
		System.out.println("Balance: "+bal);
	}

}
