package com.cloudfire.assessment0811;

public class Triangle_Circle {

	double base;
	double height;
	double area;
	double radius;
	double pi;
	double circumference;


	protected Triangle_Circle(double d,double e,double f,double g) {
		this.base=d;
		this.height=e;
		////this.area=area;
		this.radius=f;
		this.pi=g;
		//this.circumference=circumference;
	}

	protected double calculate_area_of_triangle()
	{
		// Invoke calculate area of Triangle  (base * height)/2  

		area = (base * height) / 2;
		return area;
	}
	protected double circumference_of_circle()
	{
		// Circumference of circle =   2* Math.PI * (radius)
		System.out.println("Area of Triangle: "+this.calculate_area_of_triangle());
		circumference = 2 * pi * radius;
		
		return circumference;
	}
	@Override
	public String toString() {
		return "Triangle_Circle [base=" + base + ", height=" + height + ", area=" + area + ", radius=" + radius + ", pi="
				+ pi + ", circumference=" + circumference + "]";
	}

}


