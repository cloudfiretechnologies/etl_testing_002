package com.cloudfire.assessment0811;

public class This_Arg_ConsB {
		/*
		 * 5. this: to pass as argument in the constructor call
	
	one_chain = 22 kejam
	
	one_furlong = 10 chain
	
	one_kejam = 0.9144 metre
	
	one_township = 36 sq.mile
	
		 */
	
	This_Arg_ConsA obj;
	
	This_Arg_ConsB(This_Arg_ConsA obj)
	{
		this.obj = obj;
		
	}
	
	void display()
	{
		System.out.println("1 Chain: "+obj.one_chain+"| 1 Furlong: "+obj.one_furlong+
				"| 1 Kejam: "+obj.one_kejam+"| 1 Township:  "+obj.one_township);
	}
	
}
