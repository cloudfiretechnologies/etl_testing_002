package com.cloudfire.assessment0811;

public class CarPrice {
	/*
	 * this keyword can be used to return current class instance

	Implement for the below scenario

	Initial base price of the car is 4L and the price of the car diminishes every year by 5%

	2019 , 3.8 L
	2020 , 3.6 L
	2021 , 3.2 L
	2022 , 3.0 L

	 */

	double baseprice=0.0d;
	double depreciationvalue;
	int year;
	CarPrice(double d,double e)
	{
		this.baseprice=d;
		this.depreciationvalue=e;
	}
	
	protected CarPrice carPriceValue1()
	{
			return this;
	}

	
	protected void carPriceValue(int year)
	{
		if(year==2019)
		{
			baseprice = baseprice - (baseprice * depreciationvalue*4);
			System.out.println("2019-BasePrice: "+baseprice);
		}
		else if(year==2020)
		{
			baseprice = baseprice - (baseprice * depreciationvalue*3);
			System.out.println("2020-BasePrice: "+baseprice);
		}
		else if(year==2021)
		{
			baseprice = baseprice - (baseprice * depreciationvalue*2);
			System.out.println("2021-BasePrice: "+baseprice);
		}
		else if(year==2022)
		{
			baseprice = baseprice - (baseprice * depreciationvalue);
			System.out.println("2022-BasePrice: "+baseprice);
			
		}
		else
		{
			System.out.println("Invalid");
			
		}
		
	}

}
