package com.cloudfire.assessment0811;

public class ConversionFromHectare {
	double acre;
	double cent;
	double ares;
	double squarefeet;
	double squaremetre;
	double result;
	int converted_from;
	String converted_to;
	
	
	
	ConversionFromHectare(double acre,double cent,double ares,double squarefeet,double squaremetre)
	{
		this.acre=acre;
		this.cent=cent;
		this.ares=ares;
		this.squarefeet=squarefeet;
		this.squaremetre=squaremetre;

	}
	protected void display(int converted_from, String converted_to)
	{
		switch(converted_to) {
		
		case "Acre":
			//1 Hectare = 2.47 Acre
			result = (converted_from *acre);
			System.out.println("Acres: "+result);
			break;
			
		case "Cent":
			//1 Hectare = 247 Cent
			result = (converted_from *cent);
			System.out.println("Cent: "+result);
			break;
		case "Ares":
			//1 Hectare =  100 Ares
			result = (converted_from *ares);
			System.out.println("Ares: "+result);
			break;
		case "Squarefeet":
			//1 Hectare = 107637.8 Square Feet
			result = (converted_from *squarefeet);
			System.out.println("Squarefeet: "+result);
			break;
		case "Squaremetre":
			//1 Hectare = 10,000 Square Metre
			result = (converted_from *squaremetre);
			System.out.println("Squaremetre: "+result);
			break;
			
			default:
				System.out.println("Invalid");
		
		}
	}
	@Override
	public String toString() {
		return "ConversionFromHectare [acre=" + acre + ", cent=" + cent + ", ares=" + ares + ", squarefeet="
				+ squarefeet + ", squaremetre=" + squaremetre + "]";
	}

}
