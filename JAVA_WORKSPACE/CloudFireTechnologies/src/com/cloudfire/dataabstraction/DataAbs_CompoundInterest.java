package com.cloudfire.dataabstraction;

import java.util.Scanner;

public class DataAbs_CompoundInterest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//Declare variables
		float p,n,r,t;
		System.out.println("Enter the principal: ");
		p = sc.nextFloat();//Initialise the variables
		System.out.println("Enter the rate of interest: ");
		r = sc.nextFloat();
		System.out.println("Enter the time period: ");
		t = sc.nextFloat();
		System.out.println("Enter the number of times that interest is compounded per unit t");
		n = sc.nextFloat();
		sc.close();
		
		//calculate the compound interest
		double amount = p * Math.pow(1 + (r/n),n * t);
		double compoundint = amount - p;
		System.out.println("Compound Interest "+t+ " years: "+compoundint);
		System.out.println("Amount After "+t+ " years: "+amount);
		
	}
}
