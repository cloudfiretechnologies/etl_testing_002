package com.cloudfire.constructor;

public class House_Area_Main {

	public static void main(String[] args) {

		House_Area_Calculation hc = new House_Area_Calculation(4000,36);
		System.out.println("No. of city built out of 4000 sqr miles: "+ hc.city()+" cities"
				+ " | No. of sqr miles remaining after building city: " + hc.ramainingSgrMile()+ " sqrmiles |");
		System.out.println();
		System.out.println("No. of Sqr feet from the remaining sql miles: "+hc.sqrmile_to_sqrfeet()+" sqrfeet"
				+ " | No. of house built: "+hc.noOfHouseBuilt()+ " houses");
		System.out.println();
		System.out.println("No. of sqr feet remaining after house built: "+hc.r+" sqrfeet");

	}

}
