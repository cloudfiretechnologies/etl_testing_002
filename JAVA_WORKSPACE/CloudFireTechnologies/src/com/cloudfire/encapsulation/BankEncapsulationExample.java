package com.cloudfire.encapsulation;

public class BankEncapsulationExample {
	private String acct_holdername;
	private int acct_no;
	private String bankname;
	
	protected String getAcct_holdername() {
		return acct_holdername;
	}
	protected void setAcct_holdername(String acct_holdername) {
		this.acct_holdername = acct_holdername;
	}
	protected int getAcct_no() {
		return acct_no;
	}
	protected void setAcct_no(int acct_no) {
		this.acct_no = acct_no;
	}
	String getBankname() {
		return bankname;
	}
	void setBankname(String bankname) {
		this.bankname = bankname;
	}
	
	protected BankEncapsulationExample(String acct_holdername,int acct_no,String bankname)
	{
		this.acct_holdername=acct_holdername;
		this.acct_no=acct_no;
		this.bankname=bankname;
		
	}
	
	@Override
	public String toString() {
		return "BankEncapsulationExample [acct_holdername=" + acct_holdername + ", acct_no=" + acct_no + ", bankname="
				+ bankname + ", getAcct_holdername()=" + getAcct_holdername() + ", getAcct_no()=" + getAcct_no()
				+ ", getBankname()=" + getBankname() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
}
