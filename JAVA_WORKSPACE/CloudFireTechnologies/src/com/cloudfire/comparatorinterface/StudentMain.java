package com.cloudfire.comparatorinterface;

import java.util.ArrayList;
import java.util.Collections;

public class StudentMain {

	public static void main(String[] args) {

		ArrayList<Student> al = new ArrayList<Student>();

		al.add(new Student("Anand","GBHSS",80,20));
		al.add(new Student("Kamal","ABHSS",90,22));
		al.add(new Student("Raj","PSBBHSS",70,19));
		al.add(new Student("Prakash","SBHSS",60,18));
		al.add(new Student("Hari","VVHSS",50,17));

		System.out.println("Sort By Marks: Before Sorting ");
		System.out.println("-----------------------------------------------");
		for(int i=0;i<al.size();i++)
		{
			System.out.println(al.get(i));
		}

		Collections.sort(al,new MarksComparator());
		System.out.println("Sort By Marks: After Sorting ");
		System.out.println("-----------------------------------------------");

		for(int i=0;i<al.size();i++)
		{
			System.out.println(al.get(i));
		}

		System.out.println("******************************************");

		System.out.println("Sort By Name: Before Sorting ");
		System.out.println("-----------------------------------------------");
		for(int i=0;i<al.size();i++)
		{
			System.out.println(al.get(i));
		}


		Collections.sort(al,new NameComparator());
		System.out.println("Sort By Name: After Sorting ");
		System.out.println("-----------------------------------------------");

		for(int i=0;i<al.size();i++)
		{
			System.out.println(al.get(i));
		}



	}


}
