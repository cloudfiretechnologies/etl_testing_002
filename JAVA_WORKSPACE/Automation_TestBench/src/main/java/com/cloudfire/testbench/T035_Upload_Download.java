package com.cloudfire.testbench;
import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.config.utils.Util;

public class T035_Upload_Download extends Util{

	public static void main(String[] args) throws InterruptedException {
		Util.chromeBrowserLaunch("window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login'");
		Util.orangeHRMLogin();

		List<WebElement> grid = driver.findElements(By.tagName("a"));
		for (int index = 6; index <=6; index++) {
			grid.get(index).click();
		}
		WebElement ele = driver.findElement(By.linkText("OrangeHRM, Inc"));
		ele.sendKeys(Keys.PAGE_DOWN);
		driver.findElement(By.xpath("(//button[@type='button']/i)[2]")).click();
		//Upload
		WebElement browse_btn = driver.findElement(By.xpath("//input[@type='file']"));
		browse_btn.sendKeys(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\Files\\Selenium.txt");
		//Save
		WebElement save_btn =driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
		save_btn.click();
		//Download
		WebElement download_btn = driver.findElement(By.xpath("(//div[@class='oxd-table-card']//button)[3]"));
		download_btn.click();
		//Delete uploaded files
		WebElement delete_btn = driver.findElement(By.xpath("(//div[@class='oxd-table-card']//button)[2]"));
		delete_btn.click();
		WebElement confirm_btn = driver.findElement(By.xpath("(//button)[10]"));
		confirm_btn.click();





	}

}
