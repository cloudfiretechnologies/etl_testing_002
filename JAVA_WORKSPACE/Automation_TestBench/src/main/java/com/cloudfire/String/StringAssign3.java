package com.cloudfire.String;

public class StringAssign3 {

	public static void main(String[] args) {
		
		//  3. Remove the blankspace to empty and Replace the digits. 
		  
		  String s1 = "My name is 9 Khan. My name is Bob. My name is Sonoo."; 
		  
			/*
			 * output: Mynameis9Khan.MynameisBob.MynameisSonoo. My name is 10 Khan. My name
			 * is Bob. My name is Sonoo.
			 */
		  
		  System.out.println(s1.replace(" ", ""));
		  System.out.println(s1.replaceAll("\\s", ""));
		  System.out.println(s1.replaceAll("\\d","10"));
		 
}
}

