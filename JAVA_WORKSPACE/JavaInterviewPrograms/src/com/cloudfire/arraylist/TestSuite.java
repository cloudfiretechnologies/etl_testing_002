package com.cloudfire.arraylist;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.cloudfire.unittest.UnitTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ArrayListProgram1.class,
	UnitTest.class
})	

public class TestSuite {

}
