package com.cloudfire.unittest;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class UnitTest extends TestCase {
	public int x; int y;

	public void setUp()
	{
		System.out.println("Initial Configuration ");
		x = 9;
		y = 10;
	}
	
	public void testAdder()
	{
		
		assertEquals(x+1,y);
	}
	
	
	public void tearDown()
	{
		System.out.println("After unit test this method will be loaded");
	}
	public void testArrayList()
	{
		List<String> al = new ArrayList<String>();
		al.add("Kannan");
		al.add("Raj");
		al.add("Banana");
		al.add("Apple");
		al.add(4, "Guava");
		al.addAll(0, al);
		al.remove(2);
		al.set(3, "America Apple");
		//al.removeAll(al);
		
		
		System.out.println(al);
		
	}

}
