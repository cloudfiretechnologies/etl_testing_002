package com.cloudfire.loginmodule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;


public class TC_001_forgot_pw extends configuration {
	
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
driver.get(configuration.url);
		
		WebElement username=driver.findElement(By.name("username"));
		boolean v=username.isEnabled();
		
		if(v==true)
		{
			driver.findElement(By.name("username")).sendKeys(configuration.username);
			
		}              
		else
		{
			System.out.println("invalid");
		}
		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
		for(int i=0;i<2;i++)
		{
			driver.findElement(By.name("username")).sendKeys(Keys.TAB);
		}
	
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[normalize-space(text()=' Login ')]")).sendKeys(Keys.ENTER);
		
		String Error_msg=driver.findElement(By.xpath("//span[text()='Required']")).getText();
if(Error_msg.equalsIgnoreCase("required"))
{
	System.out.println("*********************************");
	System.out.println("Test Case Pass");
}
else
{
	System.out.println("*********************************");

	System.out.println("Test Case Fail");
}

Thread.sleep(4000);
				driver.findElement(By.xpath("//div/p[text()='Forgot your password? ']")).click();
				driver.findElement(By.name("username")).sendKeys(configuration.username1);
				
				
				driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);

				
				driver.findElement(By.xpath("//div/button[normalize-space(@type=' Cancel ')]")).sendKeys(Keys.TAB);
				

driver.findElement(By.xpath("//div/button[@type='submit']")).sendKeys(Keys.ENTER);
driver.navigate().refresh();
driver.manage().window().maximize();
Thread.sleep(4000);
driver.navigate().back();
Thread.sleep(4000);
driver.navigate().forward();
String Err_msg=driver.findElement(By.xpath("//p[text()='A reset password link has been sent to you via email.']")).getText();
				if(Err_msg.equalsIgnoreCase("a reset password link has been sent to you via email."))
				{
					System.out.println("*********************************");
					System.out.println("Test case of Error Message Passed");
				}
				else
				{
					System.out.println("*********************************");

					System.out.println("Test case of Error Message failed");

				}

	}

}
