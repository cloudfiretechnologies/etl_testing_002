package com.cloudfire.loginmodule;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cloudfire.config.configuration;

public class TC016_Broken_img {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url="";
		String homepage="/web/images/";
		//HttpsURLConnection huc=null;
		ChromeOptions opt=new ChromeOptions();
		opt.addArguments("-incognito");
		opt.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(opt);
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login'");
		//driver.navigate().to(configuration.url);
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		WebElement user_name=driver.findElement(By.name("username"));
		user_name.clear();
	boolean t=	user_name.isEnabled();
	if(t==true)
	{
		System.out.println("user name validate");
		user_name.sendKeys(configuration.username);
	}
	else
	{
		System.out.println("username invalidate");
	}
WebElement p_word=driver.findElement(By.name("password"));
p_word.clear();
boolean p=p_word.isEnabled();
if(p==true)
{
	System.out.println("Password validate");
	p_word.sendKeys(configuration.password);
}
else
{
	System.out.println("invalid password");
	
}
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
driver.findElement(By.xpath("//button[@type='submit']")).sendKeys(Keys.ENTER);
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
List<WebElement> link=driver.findElements(By.tagName("img"));
int count=0;
int broken_count=0;
Iterator<WebElement> d=link.iterator();
while(d.hasNext())
{
	url=d.next().getAttribute("src");
	if(url==null || url.isEmpty())
	{
		System.out.println("URL is Empty");
		continue;
	}
	if(url.startsWith(homepage))
	{
		System.out.println("Valid url");
		continue;
	}
	try {
		HttpURLConnection huc=  (HttpURLConnection) new URL(url).openConnection();
	huc.setRequestMethod("HEAD");//sending request to HTTP server
	int response_code=huc.getResponseCode();
	if(response_code>400)
	{
		System.out.println(url+"-->Broken link");
		count=count+1;

	}
	else
	{
		System.out.println(url+"-->not a broken ink");
		broken_count=broken_count+1;

	}

	
}catch(MalformedURLException e)
	{
e.printStackTrace();
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
	

}
System.out.println("Total Non-broken Link is: "+broken_count);
System.out.println("Total Broken Link is: "+count);

	}

	
	}

