package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC020_JS_execute_loginPage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeOptions s = new ChromeOptions();
		s.addArguments("-incognito");
		s.addArguments("start-maximizied");

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(s);
		JavascriptExecutor js=(JavascriptExecutor) driver;
js.executeScript("window.location='https://shop.demoqa.com/my-account/';", "");
		//driver.navigate().to("https://shop.demoqa.com/my-account/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		int he = driver.manage().window().getSize().getHeight();
		int wid = driver.manage().window().getSize().getWidth();
		System.out.println("maximum Height and Width: " + he + "&" + wid);
		js.executeScript("window.scrollTo(0,300);","");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		WebElement user_name=driver.findElement(By.name("username"));
		js.executeScript("arguments[0].value='admin'", user_name);
		WebElement login=driver.findElement(By.name("login"));
		js.executeScript("arguments[0].click();", login);
		String name=js.executeScript("return document.title").toString();
		System.out.println("Name of the Page is: "+name);
	}

}
