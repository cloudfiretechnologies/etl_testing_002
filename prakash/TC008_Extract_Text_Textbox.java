package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class TC008_Extract_Text_Textbox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.navigate().to(configuration.url1);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		
		List<WebElement> txt_extr=driver.findElements(By.xpath("//input[@type='text' or @type='password']"));
		int a=txt_extr.size();
		
		for(int id_type=0;id_type<a;id_type++)
		{
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

			txt_extr.get(id_type).sendKeys("TEST"+id_type);
		}
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

WebElement Cnl_btn=driver.findElement(By.xpath("//input[@type='reset']"));
boolean cancel_bu=Cnl_btn.isEnabled();
if(cancel_bu==true)
{
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

	Cnl_btn.click();
	System.out.println("Validated");
}
else
{
	System.out.println("invalid option");
}
	}

}
