package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class TC011_Select_dop_down {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ChromeOptions opt=new ChromeOptions();
opt.addArguments("-incognito");
opt.addArguments("--start-maximized");
System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.navigate().to("https://demoqa.com/select-menu");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

WebElement drop_list=driver.findElement(By.cssSelector("select#oldSelectMenu"));

Select sel=new Select(drop_list);

List<WebElement> value=sel.getOptions();
for(int i=0;i<value.size();i++)
{
	System.out.println("the value is: "+value.get(i).getText());
}


  sel.selectByIndex(4);
  System.out.println("The Selected Index value :"+sel.getFirstSelectedOption().getText());
 
  sel.selectByValue("red");
  System.out.println("the selected value is: "+sel.getFirstSelectedOption(). getText());
  
  sel.selectByVisibleText("Black");
  System.out.println("The Selected Value is: "+sel.getFirstSelectedOption(). getText());
 

}
	}






