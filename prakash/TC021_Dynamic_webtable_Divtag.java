package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cloudfire.config.configuration;

public class TC021_Dynamic_webtable_Divtag {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeOptions d=new ChromeOptions();
		d.addArguments("-incognito");
		d.addArguments("--start-maximized");
		WebDriver driver=new ChromeDriver(d);
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+" \\src\\main\\resources\\Driver\\chromedriver.exe");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.navigate().to(configuration.url);
		driver.findElement(By.name("username")).sendKeys(configuration.username);
		driver.findElement(By.name("password")).sendKeys(configuration.password);
		driver.findElement(By.xpath("//div/button")).click(); 
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.findElement(By.xpath("//li/a")).click();
		
		List<WebElement> list_emp=driver.findElements(By.xpath("//div[@class='oxd-table-card']/div/div[4]"));
		List<String> emp_name=new ArrayList<String>();
		for(int emp_li=0;emp_li<list_emp.size();emp_li++)
		{
			emp_name.add(list_emp.get(emp_li).getText().trim());
			//System.out.println("Employee Name is-->"+emp_name.get(emp_li)); 

		}
		System.out.println("**********************************************");
		driver.findElement(By.xpath("(//div[@class='oxd-table-header-sort']/i)[3]")).click();
		driver.findElement(By.xpath("(//li/span[text()='Decending'])[3]")).click();
		List<WebElement> list_decend=driver.findElements(By.xpath("//div[@class='oxd-table-card']/div/div[4]")); 
		List<String> emp_decend_value=new ArrayList<String>();
		for(int emp_li2=0;emp_li2<list_decend.size();emp_li2++)
		{
			emp_decend_value.add(list_decend.get(emp_li2).getText().trim());
			//System.out.println("Employee Name after Decend----->"+emp_decend_value.get(emp_li2));
		}
		System.out.println("\n\n");
		System.out.println("######VALIDATING THE RESULTS AFTER DESCEND#######");
		for(int emp_final_list=0;emp_final_list<emp_decend_value.size();emp_final_list++)
		{
			
		if(emp_decend_value.get(emp_final_list).contains(emp_name.get(emp_final_list)))
		{
			System.out.println("\u001B"+"***************TEST CASE PASS**********"+emp_decend_value.get(emp_final_list)+"::::::::::"+emp_name.get(emp_final_list));

		}
		else
		{
			System.out.println("TEST CASE FAIL--->"+emp_decend_value.get(emp_final_list)+"::::::::::"+emp_name.get(emp_final_list));
		}
		}
		
	}

}
