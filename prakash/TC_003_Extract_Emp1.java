package com.cloudfire.loginmodule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class TC_003_Extract_Emp1 extends configuration{

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get(configuration.url);
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		WebElement user_name=driver.findElement(By.name("username"));
		user_name.clear();
	boolean t=	user_name.isEnabled();
	if(t==true)
	{
		System.out.println("user name validate");
		user_name.sendKeys(configuration.username);
	}
	else
	{
		System.out.println("username invalidate");
	}
WebElement p_word=driver.findElement(By.name("password"));
p_word.clear();
boolean p=p_word.isEnabled();
if(p==true)
{
	p_word.sendKeys(configuration.password);
}
else
{
	System.out.println("invalid password");
	
}
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
driver.findElement(By.xpath("//button[@type='submit']")).sendKeys(Keys.ENTER);
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);

driver.findElement(By.xpath("(//li/a)[2]")).sendKeys(Keys.ENTER);

WebElement h=driver.findElement(By.xpath("(//div/input)[2]"));
h.clear();
h.sendKeys(configuration.emp1);

WebElement h1=driver.findElement(By.xpath("(//div/input)[3]"));
h1.clear();
h1.sendKeys(configuration.empid);

WebElement h2=driver.findElement(By.xpath("(//div/input)[4]"));
h2.clear();
h2.sendKeys(configuration.supervisor);

Thread.sleep(4000);
driver.close();
	}
}

	

	


