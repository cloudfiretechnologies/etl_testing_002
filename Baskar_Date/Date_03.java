package com.cloudfire.Date_Calendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Date_03 {

	public static void main(String[] args) {

		SimpleDateFormat form = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = form.parse("27/03/2021");
			System.out.println(date);
		} catch (ParseException e) {

			e.printStackTrace();
		}

	}

}
