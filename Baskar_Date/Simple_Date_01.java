package com.cloudfire.Date_Calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Simple_Date_01 {

	public static void main(String[] args) {

		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		Calendar cal = Calendar.getInstance();

		try {
			cal.setTime(format.parse("03/07/1992"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		System.out.println(cal.getTime());

	}

}
