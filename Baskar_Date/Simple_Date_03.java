package com.cloudfire.Date_Calendar;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Simple_Date_03 {

	public static void main(String[] args) {

		Date date = new Date();

		String Today_Date = DateFormat.getDateInstance().format(date);
		System.out.println(Today_Date);

		String Today_Date_WithTiming = DateFormat.getDateTimeInstance().format(date);
		System.out.println(Today_Date_WithTiming);

		String C_Date = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA).format(date);
		System.out.println(C_Date);

		Locale[] localefor = DateFormat.getAvailableLocales();
		for (int index = 0; index < localefor.length; index++) {
			System.out.println(localefor[index].toString() + " " + localefor[index].getDisplayName());
		}
	}

}
