package com.cloudfire.Date_Calendar;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Date_02 {

	public static void main(String[] args) {

		Date dt = new Date();

		Locale df = Locale.FRENCH;
		DateFormat form = DateFormat.getDateInstance(DateFormat.MONTH_FIELD, df);

		System.out.println(form.format(dt));

	}
}
