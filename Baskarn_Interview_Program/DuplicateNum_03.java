package com.cloudfire.Interview_Program;

public class DuplicateNum_03 {

	protected static String Duplicate_array(int a[]) {
		
		int i, j, count;
        String out="";
		for (i = 0; i < a.length; i++) {
			count = 1;
			for (j = i + 1; j < a.length; j++) {
				if (a[i] == a[j]) {
					count++;
					a[j] = 0;
				}
			}
		
		
			if (count > 1 && a[i] != 0) {

         out=out+a[i]+" ";
			
		}
		}
		return out.trim();
	}
	
	    public static void main(String[] args) {
		int a[] = new int[] { 22, 45, 02, 89, 22, 89, 05, 44, 05 };

		System.out.println(Duplicate_array(a));

	}

}
