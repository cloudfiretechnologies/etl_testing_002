package com.cloudfire.Interview_Program;

import java.util.Scanner;

public class Leap_Year_08 {
	
	public static boolean leap_check(int year){
		
		String r="";
		if((year%4==0)&&(year%100!=0)||(year%400==0)){
			return true;																													
		}
		else{
			return false;
		}
	
	}

	public static void main(String[] args) {
		
		System.out.println("Enter the year \n ");
		try (Scanner sc = new Scanner(System.in)) {
			int year=sc.nextInt();
			System.out.println(leap_check(year));
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
