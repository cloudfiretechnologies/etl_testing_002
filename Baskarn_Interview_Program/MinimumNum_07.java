package com.cloudfire.Interview_Program;

public class MinimumNum_07 {
	
	public static String min_arr(int ar[]){
		
		int min=ar[0];
		String out_min="";
		for(int i=1;i<ar.length;i++){
			if(min>ar[i]){
				min=ar[i];
			}
		}
		out_min=out_min+min;
		return out_min;
	}
	public static void main(String[] args) {
	
    int ar[]=new int[] {03,07,92,27,02,21,04};
    System.out.println(min_arr(ar));

   }
}
